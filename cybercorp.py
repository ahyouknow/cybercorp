#!/usr/bin/python3
import sys
import os

def main():
    #lists for stats. stats are seperated whether their values are integers, percents, or strings
    genhelp()
    global statls, strStatLs, intStatLs, perStatLs, argdict
    statls = ['health', 'experience', 'level', 'cpu', 'storage', 'bandwidth', 'role', 'team' , 'class']
    strStatLs = ['role', 'team', 'class']
    intStatLs = ['health', 'experience', 'level']
    perStatLs = ['cpu', 'storage', 'bandwidth']
    #argdict tuple contains the switches and points to a stat.
    argdict = {
    ('-e', '-health'):'health',
    ('-x', '-exp'):'experience',
    ('-l', '-level'):'level',
    ('-p', '-cpu'):'cpu',
    ('-s', '-storage'):'storage',
    ('-b', '-bandwidth'):'bandwidth',
    ('-r', '-role') : 'role',
    ('-t', '-team'):'team',
    ('-c', '-class'):'class',
    }
    try:
        mainarg = sys.argv[1]
    except IndexError:
        print(shortusage)
        exit(1)
    #main arguments point to a function when the '-'s are removed
    mainargs = ['--chkstat', '--chgstat', '--add', '--remove', '--backup', '--restore']
    if mainarg == '-h' or mainarg == '--help':
        print(longhelp)
        exit(0)
    elif mainarg in mainargs:
        names = multiargs(sys.argv.index(mainarg))
    else:
        print(shortusage)
        exit(0)
    stats = {}
    #stats that are switched are put in the stats dictionary and point to a list of values if no values are put the stat points to a list with True in it
    #integer values that are put after the switch are added together
    #to put more than one argument in the list the switch must be used again
    for x in sys.argv[2:]:
        if x == '-a' or x == '-all':
            names.append('-all')
        elif x == '-stats':
            for stat in statls:
                stats = statsadd(x, stat, stats)
            break
        elif x == '-h' or x == '--help':
            print(longhelp)
            exit(0)
        for key in argdict.keys():
            if x in key:
                stats = statsadd(x, argdict[key], stats)
                break
    eval(mainarg.replace('-',''))(names,stats)
    exit(0)

#returns a list of arguments that are determined to be an argument by if there is a '-' in front of the string and it is not a digit
def multiargs(index):
    args = []
    try:
        for x in range(index+1, len(sys.argv)):
            if sys.argv[x][0] == '-' and sys.argv[x].replace('-', '').isdigit() == False:
                break
            else:
                args.append(sys.argv[x])
    finally:
        return args

#function that puts a stat in stats dictionary and points to a list with a single value
def statsadd(arg, stat, stats):
    try:
        numbers = list(map(int, multiargs(sys.argv.index(arg))))
    except ValueError:
        if stat in strStatLs:
            numbers = multiargs(sys.argv.index(arg))
        else:
            print('argument \'{}\' only accepts numbers'.format(sys.argv[sys.argv.index(arg)]))
            exit(1)
    finally:
        if stat not in stats:
            stats[stat] = []
        if not numbers:
            stats[stat].append(True)
        else:
            if stat in strStatLs:
                stats[stat].extend(numbers)
            else:
                stats[stat].append(sum(numbers))
    if sys.argv.count(arg) > 1 and len(sys.argv[sys.argv.index(arg):]) > 1:
        sys.argv.remove(arg)
    return stats

#checks for write permssions on the database
def admincheck():
    if not os.access('classdatabase.txt', os.W_OK):
        print('You need write permissions for this command')
        print(shortusage)
        exit(2)

#Check user input for anything that might break the script
def inputcheck(names):
    invalid = statls
    invalid.extend(['\n', '% ', ' ', '', 'None'])
    for arg in names:
        if arg.replace('\n', '').replace('-', '').isdigit() or arg in invalid:
            print('{} is not a valid argument'.format(arg))
            print(shortusage)
            exit(1)

def databaseSearch(names, stats):
    if not names and not stats:
        print(shortusage)
        exit(1)
    inputcheck(names)
    #if names are not used it searches for stat values
    if not names:
        for stat in stats.keys():
            names.append(str(stat+':'))
    database = open('classdatabase.txt', 'r')
    lines = database.readlines()
    database.seek(0)
    output = ''
    #names found is a list that contains the first string in each line that has matching values
    namesfound = []
    #splits each line by spaces and sees if any string is the same as the user input one
    for line in lines:
        grep = False
        linelist = list(filter(None, line.split(' ')))
        for obj in linelist:
            if obj in names or '-all' in names:
                grep = True
        if grep:
            if not stats:
                output+=line
                namesfound.append(linelist[0])
            else:
                #this part reformats the line and returns only what stats are switched on
                match = ''
                match+='{0:10} '.format(linelist[0])
                newline = (linelist[-1].replace('\n', ''))
                linelist.pop()
                linelist.append(newline)
                for stat in stats.keys():
                    index = linelist.index(stat+':')+1
                    if True in stats[stat] or str(linelist[index]) in list(map(str, stats[stat])):
                        if stat in intStatLs or stat in perStatLs:
                            match+=' {0}{1:5d}'.format(stat+':', int(linelist[index]))
                            if stat in perStatLs:
                                match+=' %'
                        else:
                            match+=' {0}{1:^12}'.format(stat+':', str(linelist[index]))
                    else:
                        match = False
                        break
                if match:
                    output+=match+'\n'
                    namesfound.append(linelist[0])
    #removes the final newline
    output = output[:-1]
    names = nameCheck(names, namesfound, output, stats)
    return names, output

#function looks at the names and stat values and sees if they were actually found in the database
#returns a list of names that were found
def nameCheck(names, namesfound, output, stats):
    #if nothing was found
    if not namesfound:
        stat = False
        for x in names:
            if x.replace(':', '') in statls:
                for stat in stats.keys():
                    if True not in stats[stat]:
                        output += '{0}:{1} '.format(stat, stats[stat])
                    elif len(stats[stat]) > 1:
                        output += '{0}:{1} '.format(stat, stats[stat].remove(True))
                print('could not find an entry with the values {}'.format(output))
                exit(1)
        if stat == False:
            print('could not find {} in database'.format(', '.join(names)))
            exit(1)
    outputlist = output.split('\n')
    #if names were found but other names might not have been found
    for x in names:
        if x.replace(':', '') in statls:
            names = namesfound
            break
        if names and len(names) != len(outputlist) and '-all' not in names:
            errornames = []
            for name in names:
                if name not in namesfound:
                    errornames.append(name)
                    names.remove(name)
            print('could not find {} in database'.format(', '.join(errornames)))
    return names

#adds a new entry to the database
def add(names, stats):
    admincheck()
    if not names:
        print('a name is needed when adding a new entry')
        exit(1)
    #puts default values in stats if they are not already set
    for stat in intStatLs+perStatLs:
        if stat not in stats.keys():
            if stat == 'experience' or stat == 'level':
                stats[stat] = [0]
            else:
                stats[stat] = [100]
    for stat in strStatLs:
        if stat not in stats.keys():
            stats[stat] = ['Null']
    databaseread = open('classdatabase.txt', 'r').read().split('\n')
    database = open('classdatabase.txt', 'a')
    namesadded = []
    for name in names:
        line = '{0:10} '.format(name)
        #this part formats each stat based on which list they are in
        for stat in statls:
            if stat in strStatLs:
                line += ' {0}:{1:^12}'.format(stat, stats[stat][0])
            elif stat in intStatLs:
                line += ' {0}:{1:5d}'.format(stat, stats[stat][0])
            elif stat in perStatLs:
                line += ' {0}:{1:5d} %'.format(stat, stats[stat][0])
        #checks to see if the line is already in the database
        #searches for lines that are exactly the same not just name
        if line in databaseread:
            print('{} is already in database'.format(name))
        else:
            database.write(line+'\n')
            namesadded.append(name)
    if namesadded:
        print('wrote student(s) {0} to database with {1} stats'.format(', '.join(namesadded), stats))
    else:
        print('No entries were added')
    database.close()
    exit(0)

def remove(names, stats):
    admincheck()

    #database wipe
    if '-all' in names:
        userInput = input('-all switch was used are you sure you want to delete everything?: [Yy/Nn]')
        if userInput == 'y' or userInput == 'Y':
            backup(names, stats)
            database = open('classdatabase.txt', 'w').close()
            print('Deleted everything in database and created a backup database')
            exit(0)
        else:
            names.remove('-all')
            print('continuing without -all switch')
    #end of database wipe

    #writes lines that do not match any lines that were found in database search
    names, output = databaseSearch(names,stats)
    linesfound = output.split('\n')
    database = open('classdatabase.txt', 'r+')
    readbackupdata = open('classdatabase.txt.bak', 'r').read().split('\n')
    lines = database.readlines()
    backupdata = open('classdatabase.txt.bak', 'a')
    database.seek(0)
    for line in lines:
        line = line.replace('\n', '')
        if line in linesfound:
            if line not in readbackupdata:
                backupdata.write(line+'\n')
        else:
            database.write(line+'\n')
    database.truncate()
    database.close()
    print('removed {} from database and put them in backup'.format(', '.join(names)))

#changes stats for entries that are found cannot change name
def chgstat(names, stats):
    admincheck()
    if not stats:
        print('--chgstat needs stat an argument')
        print(shortusage)
        exit(1)
    statschg = {}
    stats2 = {}
    used = []
    #finds what stats are used for searching or changing
    if not names:
        for x in sys.argv:
            for key in argdict.keys():
                if x in key:
                    used.append(argdict[key])
                    break
            if x == '-/':
                break
    #determines which stats are actually being changed
    #puts stats that are used for searches in stats2 else puts the stat in state2 with True
    #have fun figuring it out if you have to
    for stat in statls:
        if stat in stats.keys() and len(stats[stat]) > 1:
            used.append(stat)
        if stat not in stats.keys() or not stats[stat] or stats[stat][-1] == True:
            if stat not in strStatLs:
                statschg[stat] = 0
            else:
                statschg[stat] = None
        else:
            if len(stats[stat]) == 1 and stat in used:
                if stat not in strStatLs:
                    statschg[stat] = 0
                else:
                    statschg[stat] = None
            else:
                statschg[stat] = stats[stat][-1]
        if stat in stats and stat in used :
            if len(stats[stat]) == 1:
                stats2[stat] = [stats[stat][0]]
            else:
                stats2[stat] = stats[stat][:-1]
        else:
            stats2[stat] = [True]
    #searches database with stats2 instead of stat
    names, output = databaseSearch(names, stats2)
    output = output.split('\n')
    linechg = []
    #keeps a list of lines that were found in the searches and puts the modified line in another list
    for line in output:
        linelist = list(filter(None, line.replace('\n', '').split(' ')))
        line = '{0:10} '.format(linelist[0])
        for stat in statschg.keys():
            index = linelist.index(stat+':')+1
            if linelist[index].isdigit():
                change = str(int(linelist[index])+int(statschg[stat]))
                linelist.pop(index)
                linelist.insert(index, change)
                line+=' {0}{1:5d}'.format(stat+':', int(linelist[index]))
                if stat in perStatLs:
                    line += ' %'
            else:
                if statschg[stat] == None:
                    statschg[stat] = linelist[index]
                change = str(statschg[stat])
                linelist.pop(index)
                linelist.insert(index, change)
                line+=' {0}{1:^12}'.format(stat+':', linelist[index])
        linechg.append(line+'\n')
    database = open('classdatabase.txt', 'r+')
    lines = database.readlines()
    database.seek(0)
    #if the line is in the searched list of lines the modified version is written instead
    for line in lines:
        if line.replace('\n', '') in output:
            index = output.index(line.replace('\n', ''))
            database.write(linechg[index])
        else:
            database.write(line)
    print('changed stat entries {0} by {1}'.format(', '.join(names), statschg))
    exit(0)

def backup(names, stats):
    admincheck()
    database = open('classdatabase.txt', 'r')
    database = database.read()
    backupFile = open('classdatabase.txt.bak', 'a')
    backupFile.write(database)
    backupFile.close()
    print('completed backup')
    return

def restore(names, stats):
    admincheck()
    databaseread = open('classdatabase.txt', 'r').read().split('\n')
    database = open('classdatabase.txt', 'a')
    backuplines = open('classdatabase.txt.bak', 'r').readlines()
    for line in backuplines:
        if line.replace('\n', '') not in databaseread:
            database.write(line)
    return

#easiest function pretty much modifies the search and prints it
def chkstat(names, stats):
    names, output = databaseSearch(names,stats)
    outputlist = output.split('\n')
    #this part rearranges the lines found to match the order the user input names
    for name in names:
        if name.replace(':', '') in statls:
            break
        elif name != outputlist[names.index(name)].split(' ')[0]:
            for findingname in outputlist:
                if findingname.split(' ')[0] == name:
                    line = findingname
                    index = names.index(name)
                    break
            outputlist.remove(line)
            outputlist.insert(index, line)
    output = '\n'.join(outputlist)
    print(output)
    exit(0)

def genhelp():
    global longhelp, shortusage
    shortusage ="""usage: craftclass.py [main argument][names(s)][side arguments][stat argument]
craftclass -h or --help for more information"""

    longhelp ="""usage: craftclass.py [main argument][name(s)][side arguments][stat arguments]

main arguments:
    --chckstat            checks the database for names that match the arguments
                          and returns stats associated with those names

side arguments:
    -a, -all              specifies all entries in the database
    -stats                sets all stat switches to on. this is the same as doing \'-e -x -l -p -s -b -t -c\'

stat arguments:
    -e, -health           specifies health and when used with chkstat returns
                          with only the health associated with those names
    -x, -exp              experience
    -l, -level            level
    -p, -cpu              cpu
    -s, -storage          storage
    -b, -bandwidth        bandwidth
    -r, -role             role
    -t, -team             team
    -c, -class            class

examples:
    craftclass.py --chkstat bob jim -e -x -t
    returns:
        bob     health:  200 experience:    0 team:  foo
        jim     health:  150 experience:  100 team:  bar

    craftclass.py --chkstat -c friday -x
    returns:
	   jim      class:  friday experience:   0
       bob      class:  friday experience: 100
       moe      class:  friday experience: 250

admin commands *requires write access to database*
main arguments:
    --add                 creates a new entry in the database with default
                          stats health: 100 experience: 0 level: 0 cpu: 100
                          storage: 100 bandwidth: 100 team: Null class: Null
                          unless different values are specified
    --remove              removes an entry based on name or can be used with
                          -all to create a backup and wipe the database
    --chgstat             adds a series of numbers to entries stats can be
                          used with -all to change every entry stats use -/ when
                          finding entries by stats
    --backup              creates a backup of the current database
    --restore             overwrites the database with the backup database

examples:
    craftclass.py --add john jacob -e 120 -t foo -c friday
    creates entries:
        john    health:  120 cpu:  100 % storage:  100 %  bandwidth:  100 % experience:   0 team:  foo  class:  friday
        jacob   health:  120 cpu:  100 % storage:  100 %  bandwidth:  100 % experience:   0 team:  foo  class:  friday

    craftclass.py --chgstat john jacob -x 10 20 30 -e -15 -5
    modifies entries to:
        john    health:  100 experience:  60
        jacob   health:  100 experience:  60

    craftclass.py --chgstat -t foo -/ -c monday
    Finds every entry that is apart of team 'foo' and changes their class to 'monday'
    modifies entries to:
        john    team:  foo  class:  monday
        jacob   team:  foo  class:  monday"""
    return

if __name__ == '__main__':
    main()
